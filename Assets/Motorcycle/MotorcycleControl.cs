// Reference: https://forum.unity.com/threads/balancing-motorcycle.611017/#post-4125451

using UnityEngine;

// MS Motorcycle test - Marcos Schultz (www.schultzgames.com)
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class MotorcycleControl : MonoBehaviour
{
    WheelCollider frontCollidWheel;
    WheelCollider rearCollidWheel;
    GameObject frontMeshWheel;
    GameObject rearMeshWheel;
    Transform bodyMeshCube;
    Rigidbody rigidbody;

    float rbVelocityMagnitude;
    float horizontalInput;
    float verticalInput;
    float mediumRPM;

    void Awake()
    {
        transform.rotation = Quaternion.identity;
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.mass = 400;
        rigidbody.interpolation = RigidbodyInterpolation.Extrapolate;
        rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        //centerOfMass
        GameObject centerOfmassOBJ = new GameObject("centerOfmass");
        centerOfmassOBJ.transform.parent = transform;
        centerOfmassOBJ.transform.localPosition = new Vector3(0.0f, -0.3f, 0.0f);
        rigidbody.centerOfMass = transform.InverseTransformPoint(centerOfmassOBJ.transform.position);
        //
        BoxCollider collider = GetComponent<BoxCollider>();
        collider.size = new Vector3(0.5f, 1.0f, 3.0f);
        //
        GenerateWheels();
    }

    void OnEnable()
    {
        WheelCollider WheelColliders = GetComponentInChildren<WheelCollider>();
        WheelColliders.ConfigureVehicleSubsteps(1000, 30, 30);
    }

    void FixedUpdate()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        mediumRPM = (frontCollidWheel.rpm + rearCollidWheel.rpm) / 2;
        rbVelocityMagnitude = rigidbody.velocity.magnitude;

        //motorTorque
        if (mediumRPM > 0)
        {
            rearCollidWheel.motorTorque = verticalInput * rigidbody.mass * 4.0f;
        }
        else
        {
            rearCollidWheel.motorTorque = verticalInput * rigidbody.mass * 1.5f;
        }

        //steerAngle
        float nextAngle = horizontalInput * 35.0f;
        frontCollidWheel.steerAngle = Mathf.Lerp(frontCollidWheel.steerAngle, nextAngle, 0.125f);

        if (Mathf.Abs(rearCollidWheel.rpm) > 10000)
        {
            rearCollidWheel.motorTorque = 0.0f;
            rearCollidWheel.brakeTorque = rigidbody.mass * 5;
        }
        //
        if (rbVelocityMagnitude < 1.0f && Mathf.Abs(verticalInput) < 0.1f)
        {
            rearCollidWheel.brakeTorque = frontCollidWheel.brakeTorque = rigidbody.mass * 2.0f;
        }
        else
        {
            rearCollidWheel.brakeTorque = frontCollidWheel.brakeTorque = 0.0f;
        }
        //
        Stabilizer();
    }

    void Update()
    {
        //update wheel meshes
        Vector3 temporaryVector;
        Quaternion temporaryQuaternion;
        //
        frontCollidWheel.GetWorldPose(out temporaryVector, out temporaryQuaternion);
        frontMeshWheel.transform.position = temporaryVector;
        frontMeshWheel.transform.rotation = temporaryQuaternion;
        //
        rearCollidWheel.GetWorldPose(out temporaryVector, out temporaryQuaternion);
        rearMeshWheel.transform.position = temporaryVector;
        rearMeshWheel.transform.rotation = temporaryQuaternion;

        // lean body according to front wheel steer angle
        Quaternion bodyRotation = transform.rotation * Quaternion.Euler(0f, 0f, 
            -frontCollidWheel.steerAngle);
        bodyMeshCube.rotation = Quaternion.Lerp(bodyMeshCube.rotation, bodyRotation, 0.1f);
    }

    void Stabilizer()
    {
        Vector3 axisFromRotate = Vector3.Cross(transform.up, Vector3.up);
        Vector3 torqueForce = axisFromRotate.normalized * axisFromRotate.magnitude * 50;
        torqueForce.x = torqueForce.x * 0.4f;
        torqueForce -= rigidbody.angularVelocity;
        rigidbody.AddTorque(torqueForce * rigidbody.mass * 0.02f, ForceMode.Impulse);

        float rpmSign = Mathf.Sign(mediumRPM) * 0.02f;
        if (rbVelocityMagnitude > 1.0f && 
            frontCollidWheel.isGrounded && 
            rearCollidWheel.isGrounded)
        {
            rigidbody.angularVelocity += new Vector3(0, horizontalInput * rpmSign, 0);
        }
    }

    void GenerateWheels()
    {
        GameObject frontWeelObject = new GameObject("frontWheel");
        frontWeelObject.transform.parent = transform;
        frontWeelObject.transform.localPosition = new Vector3(0, -0.5f, 1.0f);
        frontCollidWheel = frontWeelObject.gameObject.AddComponent<WheelCollider>() as WheelCollider;
        //
        GameObject rearWheelObject = new GameObject("rearWheel");
        rearWheelObject.transform.parent = transform;
        rearWheelObject.transform.localPosition = new Vector3(0, -0.5f, -1.0f);
        rearCollidWheel = rearWheelObject.gameObject.AddComponent<WheelCollider>() as WheelCollider;

        //settings
        frontCollidWheel.mass = rearCollidWheel.mass = 40;
        frontCollidWheel.radius = rearCollidWheel.radius = 0.5f;
        frontCollidWheel.wheelDampingRate = rearCollidWheel.wheelDampingRate = 0.75f;
        frontCollidWheel.suspensionDistance = rearCollidWheel.suspensionDistance = 0.35f;
        frontCollidWheel.forceAppPointDistance = rearCollidWheel.forceAppPointDistance = 0;

        //spring
        JointSpring suspensionSpringg = new JointSpring();
        suspensionSpringg.spring = 15000;
        suspensionSpringg.damper = 4000;
        suspensionSpringg.targetPosition = 0.5f;
        frontCollidWheel.suspensionSpring = rearCollidWheel.suspensionSpring = suspensionSpringg;

        //Friction
        WheelFrictionCurve wheelFrictionCurveFW = new WheelFrictionCurve(); //friction FW
        wheelFrictionCurveFW.extremumSlip = 2.0f;
        wheelFrictionCurveFW.extremumValue = 4.0f;
        wheelFrictionCurveFW.asymptoteSlip = 4.0f;
        wheelFrictionCurveFW.asymptoteValue = 2.0f;
        wheelFrictionCurveFW.stiffness = 2.0f;
        frontCollidWheel.forwardFriction = rearCollidWheel.forwardFriction = wheelFrictionCurveFW;

        WheelFrictionCurve wheelFrictionCurveSW = new WheelFrictionCurve(); //friction SW
        wheelFrictionCurveSW.extremumSlip = 0.2f;
        wheelFrictionCurveSW.extremumValue = 1.0f;
        wheelFrictionCurveSW.asymptoteSlip = 0.5f;
        wheelFrictionCurveSW.asymptoteValue = 0.75f;
        wheelFrictionCurveSW.stiffness = 2.0f;
        frontCollidWheel.sidewaysFriction = rearCollidWheel.sidewaysFriction = wheelFrictionCurveSW;

        //generateMeshes
        frontMeshWheel = new GameObject("wheelFrontMesh");
        GameObject meshFrontTemp = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        Destroy(meshFrontTemp.GetComponent<CapsuleCollider>());
        meshFrontTemp.transform.parent = frontMeshWheel.transform;
        meshFrontTemp.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        meshFrontTemp.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
        meshFrontTemp.transform.localScale = new Vector3(1.0f, 0.1f, 1.0f);
        frontMeshWheel.transform.parent = transform;
        frontMeshWheel.transform.localPosition = new Vector3(0, -0.5f, 1.0f);
        //
        rearMeshWheel = new GameObject("wheelRearMesh");
        GameObject meshRearTemp = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        Destroy(meshRearTemp.GetComponent<CapsuleCollider>());
        meshRearTemp.transform.parent = rearMeshWheel.transform;
        meshRearTemp.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        meshRearTemp.transform.localEulerAngles = new Vector3(0.0f, 0.0f, 90.0f);
        meshRearTemp.transform.localScale = new Vector3(1.0f, 0.1f, 1.0f);
        rearMeshWheel.transform.parent = transform;
        rearMeshWheel.transform.localPosition = new Vector3(0, -0.5f, -1.0f);
        //
        bodyMeshCube = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        Destroy(bodyMeshCube.GetComponent<BoxCollider>());
        bodyMeshCube.parent = transform;
        bodyMeshCube.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        bodyMeshCube.localScale = new Vector3(0.5f, 1.0f, 3.0f);
    }
}
